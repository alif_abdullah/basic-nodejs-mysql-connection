const express = require ('express');
const mysql= require('mysql');
const app=express();



const db= mysql.createConnection( {
  host     : 'localhost',
  user     : 'root',
  password : '',
  database : 'nodemysql'
});



db.connect (function(err) {
  if(err){
    throw err;
  }
  console.log('mysql connected...');
});



//Create table in database
app.get('/createpoststable', function(req,res) {
  let sql = 'CREATE TABLE posts (id int AUTO_INCREMENT, title VARCHAR (255), body VARCHAR(255), PRIMARY KEY (id))';
  db.query(sql, function (err,result) {
    if (err){
      console.log('error');
    }
    console.log(result);
    res.send ('post table created..');
  });
});



//insert data 1 in table
app.get('/addpost1', function(req,res) {
  let post = {title: 'Post one', body: 'This is post number one'};
  let sql= 'INSERT INTO posts SET ?';
  db.query(sql, post, function (err,result) {
    if (err){
      console.log('error');
    }
    console.log(result);
    res.send('Post 1 added...');
  });
});



//insert data 2 in table
app.get('/addpost2', function(req,res) {
  let post = {title: 'Post two', body: 'This is post number two'};
  let sql= 'INSERT INTO posts SET ?';
  db.query(sql, post, function (err,result) {
    if (err){
      console.log('error');
    }
    console.log(result);
    res.send('Post 2 added...');
  });
});



//see all the data in table posts
app.get('/getposts', function(req,res) {
  let sql= 'SELECT * FROM posts';
  db.query(sql, function (err,results) {
    if (err){
      console.log('error');
    }
    console.log(results);
    res.send('Posts fetched...');
  });
});



//get posts according to the given id as param
app.get('/getpost/:id', function(req,res) {
  let sql= `SELECT * FROM posts WHERE id= ${req.params.id}`;
  db.query(sql, function (err,result) {
    if (err){
      console.log('error');
    }
    console.log(result);
    res.send('request served...');
  });
});



//update post
app.get('/updatepost/:id', function(req,res) {
  let newTitle= 'updated title'
  let sql= `UPDATE posts SET title= '${newTitle}' WHERE id= ${req.params.id}`;
  let query= db.query(sql, function (err,result) {  // let query is just the same as past statements, just query variable is having the entire obj here
    if (err){
      console.log('error');
    }
    console.log(result);
    res.send('post updated...');
  });
});





app.listen('3000', () => {
  console.log('server is on');
})


